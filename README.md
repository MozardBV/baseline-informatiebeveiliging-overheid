# @mozardbv/baseline-informatiebeveiliging-overheid

Wiki repository voor implementatie BIO.

- [Meld een bevinding](https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=367)
- [Verzoek nieuwe functionaliteit](https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=604&mDdv=990842)

## Inhoudsopgave

- [MozardBV/baseline-informatiebeveiliging-overheid](#mozardbvbaseline-informatiebeveiliging-overheid)
  - [Inhoudsopgave](#inhoudsopgave)
  - [Over dit project](#over-dit-project)
  - [Aan de slag](#aan-de-slag)
    - [MacOS](#macos)
  - [Auteurs](#auteurs)
  - [Licentie](#licentie)

## Over dit project

Wiki repository voor implementatie BIO.

De repository zelf is nagenoeg leeg. De repository bestaat feitelijk alleen voor de wiki-functionaliteit (wat zelf ook weer een Git repository is).

### Gebouwd met

- [Gollum](https://github.com/gollum/gollum) - Git-based wiki

## Aan de slag

### Afhankelijkheden

- Gollum

### Installatie

Clone de wiki:

```sh
git clone git@gitlab.com:MozardBV/baseline-informatiebeveiliging-overheid.wiki.git
```

## Gebruik

Start Gollum en bewerk lokaal:

```sh
gollum
```

## Auteurs

- **Felix Kraneveld (Mozard)** - _CISO_ - [felixkra](https://gitlab.com/felixkra)
- **Patrick Godschalk (Mozard)** - _Maintainer_ - [pgodschalk](https://gitlab.com/pgodschalk)

Zie ook de lijst van [contributors](https://gitlab.com/mozardbv/baseline-informatiebeveiliging-overheid/main) die hebben bijgedragen aan dit project.

## Licentie

[SPDX](https://spdx.org/licenses/) license: `UNLICENSED`

Copyright (c) 2006-2022 Mozard B.V.

[Leveringsvoorwaarden](https://www.mozard.nl/mozard/!suite86.scherm0325?mPag=204&mLok=1)
